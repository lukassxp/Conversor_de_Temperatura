#include <iostream>
#include "medidorTemperatura.hpp"
#include "fahrenheit.hpp"
#include "celsius.hpp"
#include "kelvin.hpp"
#include <stdlib.h>

using namespace std;

int main (void){

	int opcao = 0;
	float temp;

	Celsius* tc = new Celsius();
	Kelvin* tk = new Kelvin();
	Fahrenheit* tf = new Fahrenheit();
	
	cout << "Escalas de temperatura: \n\n\t1 - Fahrenheit\n\t2 - Celsius\n\t3 - Kelvin \n\nA temperatura esta em qual escala? ";
	cin >> opcao;
		
	cout << "\nInsira a temperatura: ";
	cin >> temp;

	switch (opcao){
		case 1: 
			tf->setTemperatura(temp);
			tk->setTemperatura(tf->converteTemperatura());
			tc->setTemperatura(tk->converteTemperatura());
			break;
		case 2: 
			tc->setTemperatura(temp);
			tf->setTemperatura(tc->converteTemperatura());
			tk->setTemperatura(tf->converteTemperatura());
			break;
		case 3: 
			tk->setTemperatura(temp);
			tc->setTemperatura(tk->converteTemperatura());
			tf->setTemperatura(tc->converteTemperatura());
			break;
	} 

	cout << "\nTemperatura em:\n\n\tFahrenheit = ";
	cout << tf->getTemperatura() << "\n\tCelsius = " << tc->getTemperatura() << "\n\tKelvin = " << tk->getTemperatura() << endl;
		 
	return 0;
}

