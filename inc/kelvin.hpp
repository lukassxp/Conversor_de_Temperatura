#ifndef KELVIN_HPP
#define KELVIN_HPP

#include "medidorTemperatura.hpp"
#include <iostream>

using namespace std;

class Kelvin : public MedidorTemperatura {
	private:

	public:
		Kelvin ();
		float converteTemperatura ();

};

#endif
