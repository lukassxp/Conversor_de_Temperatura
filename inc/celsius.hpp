#ifndef CELSIUS_HPP
#define CELSIUS_HPP

#include "medidorTemperatura.hpp"
#include <iostream>

using namespace std;

class Celsius : public MedidorTemperatura{
	private:

	public:
		Celsius ();
		float converteTemperatura ();

};

#endif
