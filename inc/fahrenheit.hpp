#ifndef FAHRENHEIT_HPP
#define FAHRENHEIT_HPP

#include "medidorTemperatura.hpp"
#include <iostream>

using namespace std;

class Fahrenheit : public MedidorTemperatura {
	private:

	public:
		Fahrenheit ();
		float converteTemperatura ();

};

#endif
