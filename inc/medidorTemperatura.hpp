#ifndef MEDIDORTEMPERATURA_HPP
#define MEDIDORTEMPERATURA_HPP

#include <iostream>

using namespace std;

class MedidorTemperatura {
	public:
		float temperatura;
		
	public:
		MedidorTemperatura ();
		void setTemperatura (float temperatura);
		float getTemperatura ();
		float converteTemperatura ();

};

#endif
